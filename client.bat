@set PROGNAME=redirect
@java -Xmn16m -Xms64m -Xms128m -Xmx512m -XX:PermSize=128m -XX:MaxPermSize=256m -Dprogram.name=%PROGNAME% -Djava.ext.dirs=lib -cp bin com.booleansoft.RedirectClient