﻿运行环境
jdk1.6+

本程序默认占用3个端口,可修改
1.http代理端口80
2.命令行本地控制端口82
3.内置数据库端口84
见文件中修改./conf/config.httf

使用说明
本程序适用于
多域名分别指向多端口

操作方法:
1.启动run.bat
2.client.bat进行添加域名 输入?进行操作提示
  例：tools add ip1=www.booleansoft.com.cn&ip2=127.0.0.1&port=8888