package com.booleansoft;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;


public class RedirectClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Socket	client=null;
		String port="82";
		if(args.length>0){
			port=args[0];
		}
		try {
			BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
			DataInputStream in=null;
			DataOutputStream out=null;
			client=new Socket("localhost",Integer.parseInt(port));
			out=new DataOutputStream(client.getOutputStream());
			in=new DataInputStream(client.getInputStream());
			out.writeUTF("login admin 74d7059c208275dc572d28192c262501");
			if("ok".equals(in.readUTF())){
				System.out.println("请输入命令!帮助[?]");
			}else{
				return;
			}
			String str="";
			while((str=reader.readLine())!=null){
				if(str.trim().equals("")){
					continue;
				}
				out.writeUTF(str);
				System.out.println(in.readUTF());
				if(str.equals("exit")){
					try {
						in.close();
					} catch (Exception e) {}
					try {
						out.close();
					} catch (Exception e) {}
					break;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				client.close();
			} catch (Exception e) {}
		}
	}

}
