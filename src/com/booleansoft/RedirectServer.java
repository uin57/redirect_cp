package com.booleansoft;

import java.io.File;
import java.lang.reflect.Method;

import com.booleansoft.util.DollClassLoader;



public class RedirectServer {
	public static String root=null;
	static{
		String rp=new File(".").getAbsolutePath();
		root = rp.replaceAll("\\.$", "")
				.replace(File.separator + "." + File.separator,
						File.separator);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
//			Class c=new DollClassLoader().loadClass("com.booleansoft.redirect.CommandServer");
			Class c=Thread.currentThread().getContextClassLoader().loadClass("com.booleansoft.redirect.CommandServer");
			Method m=c.getMethod("start");
			m.invoke(c.newInstance());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
