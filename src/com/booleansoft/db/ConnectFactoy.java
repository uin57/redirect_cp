package com.booleansoft.db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 获取数据库连接
 * @author FengWen
 *　2008-4-10
 */
public class ConnectFactoy {
	private String driverClassName;
	private String url;
	private String username;
	private String password;
	private Connection conn;
	/**
	 * 获得数据库连接
	 * @return
	 * @throws Exception 
	 */
	public Connection getConnection() throws Exception{
			Class.forName(driverClassName);
			conn=DriverManager.getConnection(url,username,password);
			return conn;
	}
	/**
	 * 关闭连接
	 */
	public void close(){
		if(conn!=null)
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getDriverClassName() {
		return driverClassName;
	}
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
