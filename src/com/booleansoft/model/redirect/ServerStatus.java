package com.booleansoft.model.redirect;

import java.util.List;
import java.util.Map;

import com.booleansoft.redirect.service.SimpleobjectService;
import com.booleansoft.util.SqlHelper;

public class ServerStatus extends SimpleEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String  corpid;
	private Integer tomcatport=-1;//tomcat服务的端口
	private Integer serverport=-1;//服务的端口
	private Integer serverstatus=0;//0,1
	public Integer getServerstatus() {
		return serverstatus;
	}
	public void setServerstatus(Integer serverstatus) {
		this.serverstatus = serverstatus;
	}
	public Integer getTomcatport() {
		return tomcatport;
	}
	public void setTomcatport(Integer tomcatport) {
		this.tomcatport = tomcatport;
	}
	public Integer getServerport() {
		return serverport;
	}
	public void setServerport(Integer serverport) {
		this.serverport = serverport;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getCorpid() {
		return corpid;
	}
	public void setCorpid(String corpid) {
		this.corpid = corpid;
	}
	public boolean isOn(){
		if(this.getServerstatus()==null){
			return false;
		}
		return this.getServerstatus()==1;
	}
	public void on(){
		this.setServerstatus(1);
		save(this);
	}
	public void off(){
		this.setServerstatus(0);
		save(this);
	}
	public static ServerStatus  get(String corpid){
		SqlHelper helper=SqlHelper.getInstance();
		Map map=helper.getQueryMap("select * from ServerStatus where corpid='"+corpid+"'");
		ServerStatus obj=new ServerStatus().fromMap(map);
		obj.setCorpid(corpid);
		return obj;
	}
	public static void save(ServerStatus obj){
		SimpleobjectService simpleobjectService=new SimpleobjectService();
		if(obj.getServerstatus()==null){
			obj.setServerstatus(0);
		}
		if(obj.getServerport()==null){
			obj.setServerport(-1);
		}
		if(obj.getTomcatport()==null){
			obj.setTomcatport(-1);
		}
		simpleobjectService.save(obj);
	}
}
