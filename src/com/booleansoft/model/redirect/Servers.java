package com.booleansoft.model.redirect;

import java.util.Date;

public class Servers extends SimpleEntity {
	private String corpid 	;// +//公司号
	private String cname	;// +//公司名称
	private String corpname	;// +//授权单位
	private String mac		;//mac地址
	private String javaoptions		;//虚拟机内存分配
	
	private String servermodel		;//模型类型
	private Integer orgs	;// +//公司数量
	private Integer users	;// +//用户数
	private Date limidate;// +//到期日期
	private String domain	;// +//域名
	
	private Integer databasemax	;// +//数据库最大连接
	private Integer databasemin	;// +//数据库最小连接
	private String databaseip	;// +//数据库ip
	private String databasedialet ;// +//数据库ip
	private String databasename	;// +//数据库名称
	
	
	private Integer databaseport	;// +//数据库端口
	private Integer workflowmodel	;// +//流程模型数
	private Integer mobilenum	;// +//客户端数量
	private Integer serverlinksmax	;// +//服务器最大连接数
	private Integer serverlinksmin	;// +//服务器最小连接数
	
	private Integer serverlinksfree	;// +//服务器空闲连接数
	private Integer fileserverlinksmax	;// +//文件服务器最大连接数
	private Integer fileserverlinksmin	;// +//文服务器最小连接数
	private Integer fileserverlinksfree	;// +//文服务器空闲数量
	private Integer attachmax ;//文服务器最小连接数
	

	private String databaseusername	;// +//数据库用户名
	private String databasepassword	;// +//数据库密码
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String getCorpid() {
		return corpid;
	}
	public void setCorpid(String corpid) {
		this.corpid = corpid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getCorpname() {
		return corpname;
	}
	public void setCorpname(String corpname) {
		this.corpname = corpname;
	}
	public Integer getOrgs() {
		return orgs;
	}
	public void setOrgs(Integer orgs) {
		this.orgs = orgs;
	}
	public Integer getUsers() {
		return users;
	}
	public void setUsers(Integer users) {
		this.users = users;
	}
	public Date getLimidate() {
		return limidate;
	}
	public void setLimidate(Date limidate) {
		this.limidate = limidate;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public Integer getDatabasemax() {
		return databasemax;
	}
	public void setDatabasemax(Integer databasemax) {
		this.databasemax = databasemax;
	}
	public Integer getDatabasemin() {
		return databasemin;
	}
	public void setDatabasemin(Integer databasemin) {
		this.databasemin = databasemin;
	}
	public String getDatabaseip() {
		return databaseip;
	}
	public void setDatabaseip(String databaseip) {
		this.databaseip = databaseip;
	}
	public String getDatabasedialet() {
		return databasedialet;
	}
	public void setDatabasedialet(String databasedialet) {
		this.databasedialet = databasedialet;
	}
	public String getDatabasename() {
		return databasename;
	}
	public void setDatabasename(String databasename) {
		this.databasename = databasename;
	}
	public Integer getDatabaseport() {
		return databaseport;
	}
	public void setDatabaseport(Integer databaseport) {
		this.databaseport = databaseport;
	}
	public Integer getWorkflowmodel() {
		return workflowmodel;
	}
	public void setWorkflowmodel(Integer workflowmodel) {
		this.workflowmodel = workflowmodel;
	}
	public Integer getMobilenum() {
		return mobilenum;
	}
	public void setMobilenum(Integer mobilenum) {
		this.mobilenum = mobilenum;
	}
	public Integer getServerlinksmax() {
		return serverlinksmax;
	}
	public void setServerlinksmax(Integer serverlinksmax) {
		this.serverlinksmax = serverlinksmax;
	}
	public Integer getServerlinksmin() {
		return serverlinksmin;
	}
	public void setServerlinksmin(Integer serverlinksmin) {
		this.serverlinksmin = serverlinksmin;
	}
	public Integer getServerlinksfree() {
		return serverlinksfree;
	}
	public void setServerlinksfree(Integer serverlinksfree) {
		this.serverlinksfree = serverlinksfree;
	}
	public Integer getFileserverlinksmax() {
		return fileserverlinksmax;
	}
	public void setFileserverlinksmax(Integer fileserverlinksmax) {
		this.fileserverlinksmax = fileserverlinksmax;
	}
	public Integer getFileserverlinksmin() {
		return fileserverlinksmin;
	}
	public void setFileserverlinksmin(Integer fileserverlinksmin) {
		this.fileserverlinksmin = fileserverlinksmin;
	}
	public Integer getFileserverlinksfree() {
		return fileserverlinksfree;
	}
	public void setFileserverlinksfree(Integer fileserverlinksfree) {
		this.fileserverlinksfree = fileserverlinksfree;
	}
	public Integer getAttachmax() {
		return attachmax;
	}
	public void setAttachmax(Integer attachmax) {
		this.attachmax = attachmax;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getJavaoptions() {
		return javaoptions;
	}
	public void setJavaoptions(String javaoptions) {
		this.javaoptions = javaoptions;
	}
	public String getServermodel() {
		return servermodel;
	}
	public void setServermodel(String servermodel) {
		this.servermodel = servermodel;
	}
	public String getDatabaseusername() {
		return databaseusername;
	}
	public void setDatabaseusername(String databaseusername) {
		this.databaseusername = databaseusername;
	}
	public String getDatabasepassword() {
		return databasepassword;
	}
	public void setDatabasepassword(String databasepassword) {
		this.databasepassword = databasepassword;
	}
	
}
