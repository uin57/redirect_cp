package com.booleansoft.model.redirect;

import java.util.Date;

public class Transmit extends SimpleEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String domain;
	private String id;
	private String ip;
	private String discribe;
	private int port; 
	
	private Date createtime;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getDiscribe() {
		return discribe;
	}
	public void setDiscribe(String discribe) {
		this.discribe = discribe;
	}

	public String[] toStrs(){
		return new String[]{this.getDomain(),this.getIp(),""+this.getPort(),this.getDiscribe()};
	}
	public String toString(){
		return  this.getDomain()+" "+this.getIp()+" "+this.getPort()+" "+this.getDiscribe();
	}
}
