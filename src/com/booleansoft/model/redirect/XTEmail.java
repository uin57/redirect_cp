package com.booleansoft.model.redirect;

public class XTEmail extends SimpleEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mailRule;
	private String smtpHost;
	private String smtpPort="25";
	private String ssl;
	private String pop3Host;
	private String pop3Port="110";
	private String username;
	private String password;
	public String getMailRule() {
		return mailRule;
	}
	public void setMailRule(String mailRule) {
		this.mailRule = mailRule;
	}
	public String getSmtpHost() {
		return smtpHost;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public String getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getPop3Host() {
		return pop3Host;
	}
	public void setPop3Host(String pop3Host) {
		this.pop3Host = pop3Host;
	}
	public String getPop3Port() {
		return pop3Port;
	}
	public void setPop3Port(String pop3Port) {
		this.pop3Port = pop3Port;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSsl() {
		return ssl;
	}
	public void setSsl(String ssl) {
		this.ssl = ssl;
	}
}
