package com.booleansoft.redirect;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class RedirectorBack implements Runnable{
	private Socket socket;
	private Socket client;
	
	public RedirectorBack(Socket socket, Socket client) {
		super();
		this.socket = socket;
		this.client = client;
	}

	public void run() {
		InputStream cin=null;
		try {
			byte [] b=new byte [2048];
			int i=0;
			OutputStream out=socket.getOutputStream();
			cin=client.getInputStream();
			while((i=cin.read(b))!=-1){
				out.write(b,0,i);
			}
		} catch (Exception e) {
		}finally{
			try {
				client.getInputStream().close();
				client.getOutputStream().close();
			} catch (Exception e) {}
			try {
				client.close();
			} catch (IOException e) {}
			try {
				socket.getInputStream().close();
				socket.getOutputStream().close();
			} catch (Exception e) {}
			try {
				socket.close();
			} catch (IOException e) {}
		}
		
	}
	
} 