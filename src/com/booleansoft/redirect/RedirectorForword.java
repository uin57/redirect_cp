package com.booleansoft.redirect;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class RedirectorForword implements Runnable{
	private Socket socket;
	private Socket client;
	
	public RedirectorForword(Socket socket, Socket client) {
		super();
		this.socket = socket;
		this.client = client;
	}

	public void run() {
		InputStream in=null;
		OutputStream cout=null;
		try {
			in = socket.getInputStream();
			byte [] b=new byte [2048];
			int i=0;
			cout=client.getOutputStream();
			while((i=in.read(b))!=-1){
				cout.write(b,0,i);
			}
		} catch (Exception e) {
		}finally{
			if(in!=null){
				try {
					in.close();
				} catch (IOException e) {}
			}
			if(cout!=null){
				try {
					cout.close();
				} catch (IOException e) {}
			}
		}
	}
	
}
