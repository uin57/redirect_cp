package com.booleansoft.redirect;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;

import com.booleansoft.model.redirect.Transmit;



public class TestRunner extends Thread{
	private int port;
	public static final List<TestRunner> Runners=new ArrayList();
	public TestRunner(int port){
		this.port=port;
	}
	ServerSocket socketServer=null;
	@Override
	public void run() {
		// TODO Auto-generated method stub
		startUp();
	}
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TestRunner.class);
	int interapte=0;
	public void startUp(){
		try {
			logger.info("启动转发器!@port="+this.port);
			System.out.println("启动转发器!@port="+this.port);
			socketServer=new ServerSocket(this.port);
			while(true){
				if(interapte==1){
					logger.info("停止转发器!");
					System.out.println("停止转发器!");
					break;
				}
				final Socket socket=socketServer.accept();
				new Thread(new Runnable(){
					public void run() {
						Socket client;
						try {
							DirectContext dc=DirectContext.getInstance();
							InputStream in=socket.getInputStream();
							ByteArrayOutputStream bos=new ByteArrayOutputStream();
							long left=4;//读4个
							byte[] b=new byte[4];
							while(left>0){
								if(left<4){
									b=new byte[(int)left];
								}
								int i=in.read(b);
								left-=i;
								bos.write(b,0,i);
							}
							IOHelper.read(in,bos, 4);
							String h=new String(bos.toByteArray()).toUpperCase();
							if(!(h.startsWith("GET")||h.startsWith("POST"))){
								socket.close();
								return;
							}
							String domain=null;
							try {
								int i=0;
								int k=0;
								while((i=in.read())!=-1){
									bos.write(i);
//									if(i==13){
//										i=in.read();
//										bos.write(i);
//										if(i==10){
//											if(++k==2){
//												break;
//											}
//										}
//									}
									System.out.print(i+" ");
								}
								
								String head=new String(bos.toByteArray());
								System.out.println(head);
								String host=head.split("\r\n")[1];
								if(host.toLowerCase().startsWith("host:")){
									domain=host.substring(5).replaceFirst("\\:.*", "").trim();
								}
								if(domain==null){
									socket.close();
									return;
								}
								
								client = new Socket("www.booleansoft.com.cn",8888);
								client.getOutputStream().write(bos.toByteArray());
								new Thread(new RedirectorForword(socket,client)).start();
								new Thread(new RedirectorBack(socket,client)).start();
							} catch (Exception e) {
							}
						} catch (Exception e) {
						}
					}
					
				}).start();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new TestRunner(81).start();
		
	}

}


