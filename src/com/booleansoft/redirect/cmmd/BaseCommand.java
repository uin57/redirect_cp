package com.booleansoft.redirect.cmmd;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseCommand {
	abstract public boolean exec(String cmd,Socket socket,DataInputStream in,
			DataOutputStream out) throws Exception;
	abstract public String getHelp();
	private static List<BaseCommand> CMDList=new ArrayList<BaseCommand>();
	static{
		add(new LoginCommand());
//		add(new ExecCommand());
	}
	public static void add(BaseCommand cmd){
		CMDList.add(cmd);
	}
	public static List<BaseCommand> getCMDList() {
		return CMDList;
	}
	public Map<String,String> getParameterMap(String param) {
		String p[]=param.split("&");
		Map<String,String>  map=new HashMap<String,String>();
		for(String s:p){
			int i=s.indexOf("=");
			if(i>-1&&s.length()>i+1){
				try{
					map.put(s.substring(0,i), java.net.URLDecoder.decode(s.substring(i+1),"UTF-8"));
				}catch(Exception e){}
			}else{
				map.put(s, "");
			}
		}
		return map;
	}
}
