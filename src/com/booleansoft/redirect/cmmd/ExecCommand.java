package com.booleansoft.redirect.cmmd;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import com.booleansoft.redirect.CommandServer;


public class ExecCommand extends BaseCommand {
	@Override
	public boolean exec(String cmd, Socket socket, DataInputStream in,
			DataOutputStream out) throws Exception {
		// TODO Auto-generated method stub
		
		if(cmd.startsWith("system ")){
			String[] params=cmd.split("\\s+",3);
			String action=params[1];
			if(action.equals("exec")){
				out.writeUTF("input cmds");
				while((cmd=in.readUTF())!=null){
					if(cmd.equals("quit")){
						out.writeUTF("ok");
						break;
					}
					try{ 
						Runtime r=Runtime.getRuntime();
						Process p=r.exec(cmd);
						out.writeUTF("ok");
					}catch(Exception e){
						out.writeUTF(e.getMessage());
					}
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CommandServer.main(args);
	}

	@Override
	public String getHelp() {
		// TODO Auto-generated method stub
		return "[system  exec]    执行本地命令\r\n";
	}

}
