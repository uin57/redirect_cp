package com.booleansoft.redirect.cmmd;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Map;

import com.booleansoft.model.redirect.Transmit;
import com.booleansoft.redirect.DirectContext;
import com.booleansoft.redirect.Runner;
import com.booleansoft.util.StringHelper;

public class LoginCommand extends BaseCommand {


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	private void startRedirect(){
		int[] ports=DirectContext.getInstance().getFromport();
		for(int port:ports){
			Runner r=new Runner(port);
			Runner.Runners.add(r);
			r.start();
		}
	}
	@Override
	public boolean exec(String cmd, Socket socket, DataInputStream in,
			DataOutputStream out) throws Exception {
		// TODO Auto-generated method stub
		boolean flag=false;
		if(cmd.toLowerCase().startsWith("tools ")){
			String params[]=cmd.split("\\s+",3);
			String action=params[1];
			if(action.equals("reload")){
				DirectContext.getInstance().loadMap();
				out.writeUTF("ok");
			}else if("shutdown".equals(action)){
				Runner.shutdownAll();
				out.writeUTF("ok");
			}else if("start".equals(action)){
				startRedirect();
				out.writeUTF("ok");
			}else if(action.equals("add")){
				Map<String,String> map=super.getParameterMap(params[2]);
				String port=map.get("port");
				String ip1=map.get("ip1");
				String ip2=map.get("ip2");
				String discribe=StringHelper.null2String(map.get("discribe"));
				try{
					int portint=Integer.parseInt(port); 
					DirectContext.getInstance().add(ip1,ip2, portint,discribe);
					out.writeUTF("ok");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					String bak="[error]the port must be integer\r\n the param may be like [add localhost localhost 8081 [描述信息]]";
					out.writeUTF(bak);
				}
			}else if(action.toLowerCase().startsWith("remove")){
				Map<String,String> map=super.getParameterMap(params[2]);
				String ip=map.get("ip");
				if("all".equals(ip.toLowerCase())){
					DirectContext.getInstance().removeAll();
				}else{
					DirectContext.getInstance().remove(ip);
				}
				out.writeUTF("ok");
			}else if(action.toLowerCase().equals("removeall")){
				DirectContext.getInstance().removeAll();
				out.writeUTF("ok");
			}else if(action.toLowerCase().endsWith("get")){
				Map<String,String> map=super.getParameterMap(params[2]);
				String ip=map.get("ip").toLowerCase();
				if("all".equals(ip)){
					String bak=DirectContext.getInstance().getAll();
					out.writeUTF(bak);
				}else{
					Transmit t=DirectContext.getInstance().get(ip);
					String bak="";
					if(t!=null){
						bak=t.toString();
					}else{
						bak="[error]"+ip+" has not bean registered";
					}
					out.writeUTF(bak);
				}
			}else if(action.toLowerCase().startsWith("getall")){
				String bak=DirectContext.getInstance().getAll();
				out.writeUTF(bak);
			}else{
				out.writeUTF(getHelp());
			}
			flag=true;
		}
		return flag;
	}
	@Override
	public String getHelp() {
		// TODO Auto-generated method stub
		return 
		"[tools start] --start the server \r\n" +
		"[tools getAll]   --get all info \r\n" +
		"[tools get ip=ip]  --get one info \r\n" +
		"[tools add ip1=ip&ip2=ip2&port=port&discribe=discribe]  --add the direct config \r\n" +
		"[tools remove ip=ip]   --remove one info \r\n" +
		"[tools removeAll]   --remove all info \r\n" +
		"[tools reload]      --reload the redirecter config \r\n" +
		"[tools shutdown]    --shutdown the redirecter\r\n";
	}

}
