package com.booleansoft.redirect.service;
 

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.util.StringHelper;

import com.booleansoft.model.redirect.SimpleEntity;
import com.booleansoft.util.NumberHelper;
import com.booleansoft.util.SqlHelper;
import com.booleansoft.util.UUIDG;

public class SimpleobjectService {
	private SqlHelper helper;
	public SimpleobjectService(){//设置默认属性
		helper=SqlHelper.getInstance();
	}
	protected final Log logger = LogFactory.getLog(getClass());
	/**
	 * 保存一条记录
	 * @param link
	 */
	public void create(SimpleEntity object){
		if(object==null){
			logger.error(object);
			return ;
		}
		String id=object.getId();
		if(StringHelper.isEmpty(id)){
			object.setId(UUIDG.getID());
		}
		String sql=helper.getInsertSql(object.getObjectType(), object.getBeanMap());
		helper.execute(sql);
		//执行插入
	}
	public void modify(SimpleEntity object){
		if(object==null){
			logger.error(object);
			return ;
		}
		String id=object.getId();
		if(StringHelper.isEmpty(id)){
			throw new RuntimeException(object.getObjectType()+"id 不能为空");
		}
		String sql=helper.getUpdateSql(object.getObjectType(),
				object.getBeanMap(), "  id='"+id+"'");
		helper.execute(sql);
	}
	/**
	 * 保存一条记录
	 * @param link
	 */
	public void save(SimpleEntity object){
		if(object==null){
			logger.error(object);
			return ;
		}
		String id=object.getId();
		if(!StringHelper.isEmpty(id)){
			int number=-1;
			try {
				number = NumberHelper.getInteger(
						helper.getValue("select count(*) c from "+object.getObjectType()
								+" where id='"+id+"'")).intValue();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(number>0){
				String sql=helper.getUpdateSql(object.getObjectType(),
						object.getBeanMap(), "  id='"+id+"'");
				helper.execute(sql);
				//执行更新
				return ;
			}
		}else{
			object.setId(UUIDG.getID());
		}
		String sql=helper.getInsertSql(object.getObjectType(), 
				object.getBeanMap());
		helper.execute(sql);
		//执行插入
	}
	public void delete(String id,Class c){
		if(StringHelper.isEmpty(id)){
			return ;
		}
		SimpleEntity s;
		try {
			s = (SimpleEntity)c.newInstance();
		} catch (Exception e) {
			logger.error(e);
			return;
		}
		helper.execute("delete from "+s.getObjectType()+" where id='"+id+"'");
	}
	
	public void delete(SimpleEntity object){
		if(StringHelper.isEmpty(object.getId())){
			return ;
		}
		helper.execute("delete from "+object.getId()+" where id='"+object.getId()+"'");
		object=null;
	}
	/**
	 * 获得yi
	 * @param id
	 * @param c
	 * @return
	 */
	public  SimpleEntity get(String id,Class c){
		if(StringHelper.isEmpty(id)){
			return null;
		}
		SimpleEntity s;
		try {
			s = (SimpleEntity)c.newInstance();
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
		List list=null;
		try {
			list = helper.getValues("select * from "+s.getObjectType()+" where id ='"+id+"'");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(list.size()==0){
			return null;
		}
		s.fromMap((Map)list.get(0));
		return s;
	}
	/**
	 * 获得yi
	 * @param id
	 * @param c
	 * @return
	 */
	public  SimpleEntity findObj(String sql,Class c){
		if(StringHelper.isEmpty(sql)){
			return null;
		}
		SimpleEntity s;
		try {
			s = (SimpleEntity)c.newInstance();
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
		List list=null;
		try {
			list = helper.getValues(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(list.size()==0){
			return null;
		}
		s.fromMap((Map)list.get(0));
		return s;
	}
}