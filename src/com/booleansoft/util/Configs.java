package com.booleansoft.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Properties;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.booleansoft.redirect.DirectContext;

public class Configs {

	private static Configs c = new Configs();
	private String settingPath;

	public String getSettingPath() {
		return settingPath;
	}

	private void Configs() {
		settingPath=DirectContext.ROOT;
	}

	public File getPathFile(String path) {
		if (path == null || path.equals("")) {
			return new File(".");
		}
		return getPathFile(new File(new File(settingPath).getAbsolutePath()
				.replaceAll("[\\\\/].$", "")), path);
	}

	public String getPath(String path) {
		if (path == null || path.equals("")) {
			return new File(".").getAbsolutePath();
		}
		String newpath = getPathFile(
				new File(new File(settingPath).getAbsolutePath().replaceAll(
						"[\\\\/].$", "")), path).getAbsolutePath();
		return newpath;
	}

	private File getPathFile(File foderfile, String path) {
		if (path == null || path.equals("")) {
			return foderfile;
		}
		if (path.startsWith("./")) {
			return new File(foderfile.getAbsolutePath() + File.separator
					+ path.substring(2));
		}
		if (path.startsWith("../")) {
			return getPathFile(
					new File(foderfile.getAbsolutePath()).getParentFile(),
					path.substring(3));
		}
		if (path.matches("^\\w{0,3}\\:.*")) {
			return new File(path);
		}
		return new File(foderfile.getAbsolutePath() + File.separator + path);
	}

	public static Configs getConfig() {
		return c;
	}

	public static Configs getConfig(String settingPath) {
		if (c.settingPath != null) {
			return c;
		}
		c.settingPath = settingPath;
		return c;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Configs c = Configs.getConfig("");
		// File root=c.getPathFile("../jdk1.5.0_14.rar");
		// String path=root.getAbsolutePath();
		// System.out.println(c.getJbossHome());
	}

	public static Properties loadConfig(String cfgname) {
		FileInputStream fis = null;
		Properties p = new Properties();
		try {
			fis = new FileInputStream(cfgname);
			p.load(fis);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return p;
	}
	
	public static Document loadDocument(String path){
		SAXReader saxReader = new SAXReader();
		Document document=null;
		try {
			document = saxReader.read(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return document;
	}
	public static void writeDocument(Document document,String path){
		BufferedWriter writer=null;
		try {
			document.setXMLEncoding("UTF-8");
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path),"UTF-8"));
			document.write(writer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(writer!=null){
				try {
					writer.close();
				} catch (IOException e) {}
			}
		}
	}
}