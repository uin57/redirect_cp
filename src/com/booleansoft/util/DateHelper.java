package com.booleansoft.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

	public static String getCurrentDate(Date date){
		if(date==null){
			return "";
		}
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}
	
	public static String getStrFromDate(Date date){
		if(date==null){
			return "";
		}
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}
	public static String getStrMSFromDate(Date date){
		if(date==null){
			return "";
		}
		return new SimpleDateFormat("yyyyMMddHHmmss").format(date);
	}
	public static Date toDate(String format,String date){
		if(StringHelper.isEmpty(date)){
			return new Date();
		}
		try {
			return new SimpleDateFormat(format).parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new Date();
	}
}
