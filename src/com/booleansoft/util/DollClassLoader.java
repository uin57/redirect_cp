package com.booleansoft.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import com.booleansoft.RedirectServer;

public class DollClassLoader extends ClassLoader{

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		System.out.println(name);
		if(name.indexOf("com.booleansoft")==-1){
			return super.findClass(name);
		}
		byte[] b=loadClassByte(name);
		Class cl=super.defineClass(name, b, 0, b.length);
		return cl;
	}
	private static Map<String,byte[]> map=new HashMap<String,byte[]>();
	static{
			Key privateKey = RSAHelper.getKey(DollClassLoader.class.getResourceAsStream("private.key"));
			File jarf=new File(RedirectServer.root+"lib"+File.separator+"dollsoft1.1c.jar");
			if(jarf.exists()){
				ZipInputStream in=null;
				try {
					in=new ZipInputStream(new FileInputStream(jarf));
					ZipEntry z=null;
					while((z=in.getNextEntry())!=null){
						if(!z.isDirectory()){
							ByteArrayOutputStream out=new ByteArrayOutputStream();
							int i=0;
							byte[] b=new byte[1024];
							while((i=in.read(b))!=-1){
								out.write(b,0,i);
							}
							byte[] nb=out.toByteArray();
							if(z.getName().endsWith(".wd")){
								nb=RSAHelper.decode(privateKey, out.toByteArray());
							}
							map.put(z.getName().replaceFirst("\\.\\w+$", "").replace("/", "."), nb);
						}
						in.closeEntry();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					if(in!=null){
						try {
							in.close();
						} catch (Exception e) {}
					}
				}
			}
	}
	protected byte[] loadClassByte(String name){
		return map.get(name);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Class c=new DollClassLoader().loadClass("com.booleansoft.redirect.CommandServer");
			Method m=c.getMethod("start");
			m.invoke(c.newInstance());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
