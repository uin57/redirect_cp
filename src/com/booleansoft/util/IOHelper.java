package com.booleansoft.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class IOHelper {
	private final static int BUFFER_SIZE=2048;
	public static void read(InputStream in,OutputStream out,long size) throws Exception{
		long left=size;
		byte[] b=new byte[BUFFER_SIZE];
		while(left>0){
			if(left<BUFFER_SIZE){
				b=new byte[(int)left];
			}
			left-=in.read(b);
			out.write(b);
		}
	}
	public static byte[] readByte(InputStream in,long size) throws Exception{
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		read(in,out,size);
		return out.toByteArray();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
