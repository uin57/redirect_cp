package com.booleansoft.util;

import java.text.NumberFormat;

public class NumberHelper {

	//格式化
	public static String format(Double d,int n){
		if(d==null){
			d=0.00d;
		}
		NumberFormat format = NumberFormat.getInstance();
		format.setMaximumFractionDigits(n);
		format.setMinimumFractionDigits(n);
		return format.format(d);
	}
	/**
	 * 获得int值
	 * 
	 * @param o
	 * @param d
	 * @return
	 */
	public static int getIntValue(Object o, int d) {
		try {
			return Integer.parseInt(""+o);
		} catch (Exception e) {
		}
		return d;
	}

	/**
	 * 获得int值, 默认为0
	 * 
	 * @param o
	 * @return
	 */
	public static int getIntValue(Object o) {
		return getIntValue(o, 0);
	}

	/**
	 * 获得int值, 默认为0
	 * 
	 * @param o
	 * @return
	 */
	public static Integer getInteger(Object o) {
		if (o == null) {
			return null;
		}
		if (o instanceof Number) {
			return ((Number) o).intValue();
		}
		if (("" + o).equals("null")) {
			return null;
		}
		int i = 0;
		try {
			i = Integer.parseInt("" + o);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return i;
	}

	/**
	 * 获得long值, 默认为0
	 * 
	 * @param o
	 * @return
	 */
	public static long getLongValue(Object o) {
		return getLong(o);
	}

	/**
	 * 获得int值, 默认为0
	 * 
	 * @param o
	 * @return
	 */
	public static Long getLong(Object o) {
		if (o == null) {
			return null;
		}
		if (o instanceof Number) {
			return ((Number) o).longValue();
		}
		if (("" + o).equals("null")) {
			return null;
		}
		long i = 0L;
		try {
			i = Long.parseLong("" + o);
		} catch (NumberFormatException e) {}
		return i;
	}
	/**
	 * 获得long值, 默认为0
	 * 
	 * @param o
	 * @return
	 */
	public static long getLongValue(Object o,long value) {
		try {
			return Long.parseLong(""+o);
		} catch (Exception e) {}
		return value;
	}

	/**
	 * 获得double值
	 * 
	 * @param o
	 * @return
	 */
	public static double getDoubleValue(Object o) {
		Double d = getDouble(o);
		return d == null ? 0.00d : d.doubleValue();
	}
	/**
	 * 获得double值
	 * 
	 * @param o
	 * @return
	 */
	public static double getDoubleValue(Object o,double dd) {
		Double d = getDouble(o);
		return d == null ? dd : d.doubleValue();
	}

	/**
	 * 获得double值
	 * 
	 * @param o
	 * @return
	 */
	public static Double getDouble(Object o) {
		if (o == null) {
			return null;
		}
		if (o instanceof Number) {
			return ((Number) o).doubleValue();
		}
		double d = 0.00d;
		try {
			d = Double.parseDouble("" + o);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return d;
	}

	/**
	 * 排序
	 * 
	 * @param onumber
	 * @param asc
	 *            是否升序
	 * @return
	 */
	public static Integer[] sort(Integer[] onumber, boolean asc) {
		Integer[] nums = new Integer[onumber.length];
		System.arraycopy(onumber, 0, nums, 0, onumber.length);
		for (int j = 1; j < nums.length; j++) {
			for (int i = 1; i <= nums.length - j; i++) {
				int o = nums[i - 1].intValue();
				int p = nums[i].intValue();
				if (asc && o > p) {
					nums[i] = o;
					nums[i - 1] = p;
				} else if (!asc && p > o) {
					nums[i] = o;
					nums[i - 1] = p;
				}
			}
		}
		return nums;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(format(22*0.1/3,5));

	}
}