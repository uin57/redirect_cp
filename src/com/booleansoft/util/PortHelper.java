package com.booleansoft.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.booleansoft.RedirectServer;

public class PortHelper {
	static{
//		File jarf=new File(RedirectServer.root+"lib"+File.separator+"dollsoft1.1c.jar");
//		ZipInputStream in=null;
//		ZipOutputStream out=null;
//		try {
//			String keystr="";
//			in=new ZipInputStream(new FileInputStream(jarf));
//			ZipEntry z=null;
//			while((z=in.getNextEntry())!=null){
//				if(z.getName().endsWith("doll.key")){
//					int i=0;
//					byte[]b=new byte[1024];
//					ByteArrayOutputStream bos=new ByteArrayOutputStream();
//					while((i=in.read(b))!=-1){
//						bos.write(b,0,i);
//					}
//					byte[] ob=bos.toByteArray();
//					byte[] db=new byte[bos.toByteArray().length];
//					for(int k=0;k<ob.length;k++){
//						db[k]=(byte)(ob[k]+1);
//					}
//					keystr=new String(db);
//				}
//				in.closeEntry();
//			}
//			String rtime="";
//			String rmac="";
//			String rcode="";
//			String[] k=keystr.split(" ");
//			
////			out=new ZipOutputStream(new FileOutputStream(jarf));
////			ZipEntry zip=new ZipEntry("com/booleansoft/util/doll.key");
////			out.putNextEntry(zip);
////			out.write();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}finally{
//			if(in!=null){
//				try {
//					in.close();
//				} catch (Exception e) {}
//			}
//		}
//		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getMac());
	}
	public static String encodeStr(String str){
		byte[] b=str.getBytes();
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<b.length;i++){
			sb.append(Integer.toHexString(b[i]));
		}
		return sb.toString();
	}
	public static String decodeStr(String str){
		int len=str.length()/2;
		byte[] dbs=new byte[len];
		for(int i=0;i<len;i++){
			dbs[i]=(byte)Integer.parseInt(str.charAt(2*i)+""+str.charAt(2*i+1),16);
		}
		return new String(dbs);
	}
	public static int isOn(int port) {
		Socket client=null;
		try {
			client = new Socket("127.0.0.1",port);
		} catch (UnknownHostException e) {
			return -2;
		} catch (IOException e) {
			return -1;
		}finally{
			if(client!=null){
				try {
					client.close();
				} catch (IOException e) {}
			}
		}
		return 1;
	}
	public static synchronized int getAvaliPort(){
		while(isOn(i)>0){
			i++;
		}
		return i++;
	}
	public static volatile int i=2000;
	public static String[] getMac(){
		try {
			Process p=Runtime.getRuntime().exec("cmd /c ipconfig/all");
			ByteArrayOutputStream bos=new ByteArrayOutputStream();
			InputStream in=p.getInputStream();
			int i=0;
			byte[]b =new byte[1024];
			while((i=in.read(b))!=-1){
				bos.write(b,0,i);
			}
			in.close();
			String[] str=new String(bos.toByteArray()).split("\r\n");
			StringBuffer sb=new StringBuffer();
			for(String s:str){
				if(s.indexOf("Physical Address")>-1){
					sb.append(s.substring(s.lastIndexOf(":")+1).trim()).append(",");
				}
			}
			return sb.toString().replaceFirst(",$", "").split(",");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
