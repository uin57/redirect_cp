package com.booleansoft.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringHelper {
	public static final String CODE_UTF8="UTF-8";
	/**
	 * 是否为空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		if (str == null || str.equals("null")) {
			return true;
		}
		if (str.trim().equals("")) {
			return true;
		}
		return false;
	}

	/**
	 * 非空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	/**
	 * 去空
	 * 
	 * @param object
	 * @return
	 */
	public static String null2String(Object object) {
		if (object == null) {
			return "";
		}
		if(object.equals("null")){
			return "";
		}
		return ("" + object).trim();
	}

	/**
	 * 截取
	 * 
	 * @param object
	 * @return
	 */
	public static String getMoreStr(String str,int len) {
		if(StringHelper.isEmpty(str)){
			return "";
		}
		if(str.length()<len){
			return str;
		}
		return str.substring(0,len);
	}
	/**
	 * 截取
	 * 
	 * @param object
	 * @return
	 */
	public static String getSplitMoreStr(String str,int len) {
		if(StringHelper.isEmpty(str)){
			return "";
		}
		str=str.trim();
		String[] strs=str.split("\\s+");
		if(strs.length<=len){
			return str;
		}
		StringBuffer sb=new StringBuffer();
		int i=0;
		for(String s:strs){
			sb.append(s).append(" ");
			if(++i>=len){
				break;
			}
		}
		return sb.toString().trim()+"...";
	}
	/**
	 * 
	 * 
	 * @param object
	 * @return
	 */
	public static String null2String(Object object, String defaultstr) {
		if (object == null) {
			return defaultstr;
		}
		if (StringHelper.isEmpty("" + object)) {
			return defaultstr;
		}
		return ("" + object).trim();
	}
	/**
	 * 单个字符串加密
	 * @param str
	 * @param code
	 * @return
	 */
	public static String encodeStr(String str){
		return StringHelper.encodeStr(str,"UTF-8");
	}
	
	/**
	 * 单个字符串加密
	 * @param str
	 * @param code
	 * @return
	 */
	public static String encodeXTURL(String str){
		if(str==null){
			return "";
		}
		return str.replace("=", "$XT_EQUEALS_XT_EQUEALS_XT$").replace("&", "$XT_AND_XT_AND_XT$");
	}
	/**
	 * 单个字符串加密
	 * @param str
	 * @param code
	 * @return
	 */
	public static String decodeXTURL(String str){
		if(str==null){
			return "";
		}
		return str.replace("$XT_EQUEALS_XT_EQUEALS_XT$","=").replace("$XT_AND_XT_AND_XT$","&");
	}
	
	/**
	 * 单个字符串加密
	 * @param str
	 * @param code
	 * @return
	 */
	public static String encodeStr(String str,String code){
		code=StringHelper.null2String(code,"UTF-8");
		try {
			return java.net.URLEncoder.encode(str,code);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	/**
	 * 单个字符串解密
	 * @param str
	 * @param code
	 * @return
	 */
	public static String decodeStr(String str,String code){
		code=StringHelper.null2String(code,"UTF-8");
		try {
			return java.net.URLDecoder.decode(str,code);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	/**
	 * URL加密
	 * @param url
	 * @param code
	 * @return
	 */
	public static String encodeURL(String url,String code){
		if(url==null){
			return null;
		}
		
		StringBuffer sb=new StringBuffer();
		int subs=url.indexOf("?");
		if(subs==-1){
			return url;
		} 
		sb.append(url.substring(0,subs));
		String paramstr[]=url.substring(subs).split("\\&");
		for(String s:paramstr){
			if(StringHelper.isEmpty(s)){
				continue;
			}
			sb.append(s.replaceAll("\\=[\\s\\S]*", "=")); 
			String codestr=s.replaceFirst("[^\\=]*\\=([\\s\\S]*)", "$1");
			Pattern p = Pattern.compile("[^\\x00-\\xff]*");
			Matcher m = p.matcher(codestr);
			StringBuffer innersb = new StringBuffer();
			while (m.find()) {
			    m.appendReplacement(innersb, encodeStr(m.group(),code));
			}
			m.appendTail(innersb);
			sb.append(innersb.toString()); 
			sb.append("&");
		} 
		return sb.toString().replaceFirst("\\&$", "");
	}
	public static String trimToNull(String str){
		return isEmpty(str)?null:str;
	}
	public static List<String> split(String str,String spl){
		if(str==null){
			return new ArrayList<String>();
		}
		String arr[]=str.split(spl);
		List<String> list=new ArrayList<String>();
		for(String s:arr){
			if(!StringHelper.isEmpty(str)){
				list.add(s);
			}
		}
		return list;
	}
	public static String join(List list,String str){
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<list.size();i++){
			String id=(String)list.get(i);
			if(StringHelper.isEmpty(id)){
				continue;
			}
			sb.append(id);
			if(i<list.size()-1){
				sb.append(str);
			}
		}
		return sb.toString();
	}
	public static String joinSql(List<String> list){
		return "'','"+join(list,"','")+"'";
	}
	
}