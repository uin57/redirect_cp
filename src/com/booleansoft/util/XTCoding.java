package com.booleansoft.util;

import java.security.MessageDigest;
import java.util.UUID;

public class XTCoding {

	public static String encodeMD5(String string){
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9','a', 'b', 'c', 'd','e', 'f' };
        try {
            byte[] bytes = string.getBytes();
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(bytes);
            byte[] updateBytes = messageDigest.digest();
            int len = updateBytes.length;
            char myChar[] = new char[len * 2];
            int k = 0;
            for (int i = 0; i < len; i++) {
                byte byte0 = updateBytes[i];
                myChar[k++] = hexDigits[byte0 >>> 4 & 0x0f];
                myChar[k++] = hexDigits[byte0 & 0x0f];
            }
            return new String(myChar);
        } catch (Exception e) {
        	return null;
        }
    }
	public static boolean check(String str){
		String str1=str.substring(0,4);
		String str2=str.substring(4);
		String enstr=encodeMD5(str1);
		return enstr.substring(4).equals(str2);
	}
	public static String getCoding(){
		String id=UUID.randomUUID().toString().replace("-", "");
		String str1=id.substring(0,4);
		String str2=encodeMD5(str1).substring(4);
		return str1+str2;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str1=getCoding();
		System.out.println(str1);
		System.out.println(""+check(str1));
	}

}
